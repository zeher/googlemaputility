//
//  PlaceTableViewCell.h
//  GoogleMapUtility
//
//  Created by Настя on 21/06/15.
//  Copyright (c) 2015 Nastya. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PlaceTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *placeName;
@property (weak, nonatomic) IBOutlet UILabel *distance;

@end
