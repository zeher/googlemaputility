//
//  RequestManager.m
//  GoogleMapUtility
//
//  Created by Настя on 20/06/15.
//  Copyright (c) 2015 Nastya. All rights reserved.
//

#import "RequestManager.h"
#import <GoogleMaps/GoogleMaps.h>
#import "PlaceObject.h"
#import "NSArray+Extensions.h"


@implementation RequestManager

+ (AFHTTPRequestOperation *)nearbyPlacesForLocation:(CLLocation *)location withBlock:(void (^)(NSArray *, NSError *))block {
	
	AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
	NSDictionary *parameters = @{
								 @"key": @"AIzaSyCx85ZWFQ8v11yIsavmq4T_5AsQiIbcKsQ",
								 @"location": [NSString stringWithFormat:@"%lf,%lf", location.coordinate.latitude, location.coordinate.longitude],
								 @"radius": @"5000"
								 };
	return [manager GET:@"https://maps.googleapis.com/maps/api/place/nearbysearch/json" parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject) {

		NSArray *places = cmap(responseObject[@"results"])(^id(NSDictionary *object) {
			
			return [PlaceObject createWithDictionary:object];
		});
		block(places, nil);
		
	} failure:^(AFHTTPRequestOperation *operation, NSError *error) {
		block(nil, error);
	}];
}

@end
