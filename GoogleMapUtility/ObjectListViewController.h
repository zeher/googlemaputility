//
//  ObjectListViewController.h
//  GoogleMapUtility
//
//  Created by Настя on 20/06/15.
//  Copyright (c) 2015 Nastya. All rights reserved.
//

#import <UIKit/UIKit.h>
@import CoreLocation;

@interface ObjectListViewController : UIViewController

@property (nonatomic) CLLocation *location;

@end
