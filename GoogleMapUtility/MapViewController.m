//
//  MapViewController.m
//  GoogleMapUtility
//
//  Created by Настя on 20/06/15.
//  Copyright (c) 2015 Nastya. All rights reserved.
//

#import "MapViewController.h"
#import <GoogleMaps/GoogleMaps.h>

@interface MapViewController () <GMSMapViewDelegate>

@end

@implementation MapViewController

- (void)viewDidLoad {
    [super viewDidLoad];
	
	self.mapView.myLocationEnabled = YES;
	self.mapView.settings.compassButton = YES;
	self.mapView.settings.myLocationButton = YES;
	self.mapView.delegate = self;
	
	[self.mapView addObserver:self
				   forKeyPath:@"myLocation"
					  options:NSKeyValueObservingOptionNew
					  context:NULL];
	
}

- (void)dealloc {
	[self.mapView removeObserver:self forKeyPath:@"myLocation"];
}

- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context {
	
	CLLocation *location = [change objectForKey:NSKeyValueChangeNewKey];
	self.mapView.camera = [GMSCameraPosition cameraWithTarget:location.coordinate
														 zoom:16];

}


@end
