//
//  PlaceObject.h
//  GoogleMapUtility
//
//  Created by Настя on 20/06/15.
//  Copyright (c) 2015 Nastya. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <PromiseKit/PromiseKit.h>
@class CLLocation;

@interface PlaceObject : NSObject

@property (nonatomic) NSString *name;
@property (nonatomic) CLLocation *location;
@property (nonatomic) BOOL hasViewport;

+ (instancetype)createWithDictionary:(NSDictionary *)object;
+ (PMKPromise *)loadObjectsNearLocation:(CLLocation *)location;

@end
