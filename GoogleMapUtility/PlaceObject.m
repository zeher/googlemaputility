//
//  PlaceObject.m
//  GoogleMapUtility
//
//  Created by Настя on 20/06/15.
//  Copyright (c) 2015 Nastya. All rights reserved.
//

#import "PlaceObject.h"
#import "RequestManager.h"
@import CoreLocation;


@implementation PlaceObject

+ (PlaceObject *)createWithDictionary:(NSDictionary *)object {
	
	PlaceObject *place = PlaceObject.new;
	place.name = object[@"name"];
	double latitude = [object[@"geometry"][@"location"][@"lat"] doubleValue];
	double longitude = [object[@"geometry"][@"location"][@"lng"] doubleValue];
	place.location = [[CLLocation alloc] initWithLatitude:latitude longitude:longitude];
	place.hasViewport = !!object[@"geometry"][@"viewport"];
	
	return place;
}

+ (PMKPromise *)loadObjectsNearLocation:(CLLocation *)location {
	
	return [PMKPromise promiseWithResolver:^(PMKResolver resolve) {
		
		[RequestManager nearbyPlacesForLocation:location withBlock:^(NSArray *places, NSError *error) {
			resolve(error ?: places);
		}];
	}];
}

@end
