//
//  ObjectListViewController.m
//  GoogleMapUtility
//
//  Created by Настя on 20/06/15.
//  Copyright (c) 2015 Nastya. All rights reserved.
//

#import "ObjectListViewController.h"
#import "RequestManager.h"
#import "PlaceObject.h"
#import <PromiseKit/PromiseKit.h>
#import "PlaceTableViewCell.h"

@interface ObjectListViewController ()<UITableViewDataSource, UITableViewDelegate>
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (nonatomic) NSArray *dataSource;

@end

@implementation ObjectListViewController

- (void)viewDidLoad {
    [super viewDidLoad];
	
	self.tableView.tableFooterView = UIView.new;
	self.tableView.estimatedRowHeight = 44.0;
	self.tableView.rowHeight = UITableViewAutomaticDimension;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
	PlaceTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell" forIndexPath:indexPath];
	PlaceObject *place = self.dataSource[indexPath.row];
	cell.placeName.text = place.name;
	
	NSString *detailText = !place.hasViewport ? [NSString stringWithFormat:@"%.1lf км.", [place.location distanceFromLocation:self.location] / 1000] : nil;
	cell.distance.text = detailText;
	
	return cell;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
	return self.dataSource.count;
}

- (void)setLocation:(CLLocation *)location {
	_location = location;
	[PlaceObject loadObjectsNearLocation:location].then(^(NSArray *places) {
		
		self.dataSource = places;
		[self.tableView reloadData];
	});
}

@end
