//
//  MapViewController.h
//  GoogleMapUtility
//
//  Created by Настя on 20/06/15.
//  Copyright (c) 2015 Nastya. All rights reserved.
//

#import <UIKit/UIKit.h>

@class GMSMapView;
@interface MapViewController : UIViewController

@property (weak, nonatomic) IBOutlet GMSMapView *mapView;

@end
