//
//  NSArray+EXtensions.m
//  GoogleMapUtility
//
//  Created by Настя on 20/06/15.
//  Copyright (c) 2015 Nastya. All rights reserved.
//

#import "NSArray+Extensions.h"

@implementation NSArray (Extensions)

- (MapFunction)map {
	return ^(id(^block)(id)) {
		
		id mapped[self.count];
		NSUInteger i = 0;
		for (id mappable in self) {
			id o = block(mappable);
			if (o) mapped[i++] = o;
		}
		return [NSArray arrayWithObjects:mapped count:i];
	};
}

@end


MapFunction cmap(NSArray *collection) {
	if (collection)
		return collection.map;
	else
		return ^(id _) { return @[]; };
}
