//
//  ViewController.m
//  GoogleMapUtility
//
//  Created by Настя on 18/06/15.
//  Copyright (c) 2015 Nastya. All rights reserved.
//

#import "ViewController.h"
#import "MapViewController.h"
#import "ObjectListViewController.h"
#import <GoogleMaps/GoogleMaps.h>

@interface ViewController ()

@property (weak, nonatomic) IBOutlet UIView *containerView;
@property (weak, nonatomic) IBOutlet UISegmentedControl *segmentedControl;

@property (nonatomic) UIViewController *currentViewController;
@property (nonatomic) MapViewController *mapViewController;
@property (nonatomic) ObjectListViewController *objectListViewController;

@end

@implementation ViewController

- (void)viewDidLoad {
	[super viewDidLoad];
	[self changeView:self.segmentedControl];
	
	self.mapViewController = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"MapView"];
	self.mapViewController.view.frame = self.containerView.bounds;
	[self addChildViewController:self.mapViewController];
	[self.containerView addSubview:self.mapViewController.view];
	[self.mapViewController didMoveToParentViewController:self];
	
	self.objectListViewController = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"ObjectListView"];
	self.objectListViewController.view.frame = self.containerView.bounds;
	[self addChildViewController:self.objectListViewController];
	[self.containerView addSubview:self.objectListViewController.view];
	[self.objectListViewController didMoveToParentViewController:self];
	self.objectListViewController.view.hidden = YES;
}

- (IBAction)changeView:(UISegmentedControl *)sender {
	
	if (sender.selectedSegmentIndex == 1)
		self.objectListViewController.location = self.mapViewController.mapView.myLocation;
	
	self.mapViewController.view.hidden = !!sender.selectedSegmentIndex;
	self.objectListViewController.view.hidden = !sender.selectedSegmentIndex;
	
}

@end
