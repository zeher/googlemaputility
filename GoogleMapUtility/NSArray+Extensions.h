//
//  NSArray+EXtensions.h
//  GoogleMapUtility
//
//  Created by Настя on 20/06/15.
//  Copyright (c) 2015 Nastya. All rights reserved.
//

#import <Foundation/Foundation.h>
typedef NSArray *(^MapFunction)(id(^)(id));

@interface NSArray (Extensions)

- (MapFunction)map;

@end

extern MapFunction cmap(NSArray *);
