//
//  RequestManager.h
//  GoogleMapUtility
//
//  Created by Настя on 20/06/15.
//  Copyright (c) 2015 Nastya. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AFNetworking.h"
@import CoreLocation;

@interface RequestManager : NSObject

+ (AFHTTPRequestOperation *)nearbyPlacesForLocation:(CLLocation *)location withBlock:(void(^)(NSArray *places, NSError *error))block;

@end
